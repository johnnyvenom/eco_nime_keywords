Review of environmental topics in NIME literature
================================================

In preparation for NIME 2021 paper submission with R Masu, A. Pultz and A. Jensenius. 

## Overview: 

There seems to be little mention environmental sustainability in existing NIME publications. To assess this, a short review of NIME proceedings is undertaken. Drawing from methods used by Jensenius (2014) to investigate the term `gesture` in NIME, and subsequently adapted by Sullivan & Wanderley (2018) to investigate issues of DMI reliability in literature, a keyword search is run on the corpus of NIME proceedings to check fro usage of words related to environmental issues. Basic metrics are retrieved, and then relevant papers are identified and summarily reviewed for further context. Additional steps may include concordance and collocation analyses as well. 

## Initial results

Using a shell script that employs the `mdfind` terminal command, the following keywords were searched for in the corpus: 

- ***environment***, ***sustainability***, ***conservation***, ***ecology***, ***pollution***, ***carbon***, ***footprint***, ***biodiversity***, ***climate***

The following table shows the occurrences (number of papers in which each term is contained) by year: 

| year | environment | sustainability | conservation | ecology | pollution | carbon | footprint | biodiversity | climate |
|:----:|:----:|:--:|:-:|:--:|:--:|:--:|:--:|:-:|:--:|
| 2001 | 8    | 0  | 0 | 0  | 0  | 0  | 0  | 0 | 0  |
| 2002 | 30   | 0  | 0 | 0  | 1  | 2  | 0  | 0 | 0  |
| 2003 | 28   | 0  | 0 | 0  | 1  | 2  | 1  | 0 | 0  |
| 2004 | 35   | 0  | 0 | 0  | 1  | 0  | 1  | 0 | 0  |
| 2005 | 47   | 0  | 0 | 1  | 0  | 0  | 0  | 0 | 0  |
| 2006 | 55   | 0  | 0 | 0  | 2  | 2  | 0  | 0 | 0  |
| 2007 | 60   | 1  | 0 | 3  | 0  | 4  | 1  | 0 | 0  |
| 2008 | 59   | 1  | 0 | 1  | 0  | 4  | 3  | 0 | 0  |
| 2009 | 54   | 0  | 0 | 4  | 0  | 5  | 0  | 0 | 0  |
| 2010 | 69   | 1  | 0 | 1  | 2  | 1  | 1  | 0 | 2  |
| 2011 | 82   | 1  | 0 | 6  | 0  | 3  | 2  | 0 | 0  |
| 2012 | 76   | 1  | 0 | 4  | 1  | 2  | 6  | 0 | 1  |
| 2013 | 70   | 0  | 0 | 2  | 1  | 1  | 2  | 0 | 1  |
| 2014 | 89   | 0  | 1 | 8  | 1  | 3  | 4  | 0 | 1  |
| 2015 | 46   | 1  | 0 | 5  | 0  | 0  | 2  | 1 | 1  |
| 2016 | 62   | 2  | 1 | 4  | 0  | 2  | 1  | 0 | 1  |
| 2017 | 69   | 1  | 0 | 9  | 1  | 2  | 3  | 0 | 3  |
| 2018 | 62   | 1  | 0 | 4  | 0  | 3  | 3  | 0 | 0  |
| 2019 | 54   | 3  | 0 | 4  | 0  | 3  | 0  | 0 | 1  |
| 2020 | 89   | 4  | 2 | 12 | 0  | 6  | 4  | 1 | 3  |
| **TOTALS**| **1144** | **17** | **4** | **68** | **11** | **45** | **34** | **2** | **14** |

Obviously, the keywords may have different meanings, especially in the case of `environment`, which is used in over 50% of all NIME papers. To identify a small subset of papers to initially examine, the following table shows the most salient results: 

| # keywords | # papers | filenames | 
|:--:|:----:|-------------------------------|
| 5  | 1    | [nime2020_paper97.pdf][2020-097]     | 
| 4  | 4    | [nime2012_264.pdf][2012-264]         |
|    |      | [nime2015_325.pdf][2015-325]         |
|    |      | [nime2017_paper0059.pdf][2017-059]   |
|    |      | [nime2020_paper31.pdf][2020-031]     |
| 3  | 10   | [nime2006_396.pdf][2006-396]         |     
|    |      | [nime2008_019.pdf][2008-019]         |
|    |      | [nime2010_427.pdf][2010-427]         |
|    |      | [nime2011_028.pdf][2011-028]         |
|    |      | [nime2016_paper0028.pdf][2016-028]   |
|    |      | [nime2017_paper0031.pdf][2017-031]   |
|    |      | [nime2018_paper0040.pdf][2018-040]   |
|    |      | [nime2019_paper024.pdf][2019-024]    |
|    |      | [nime2020_paper46.pdf][2020-046]     |
|    |      | [nime2020_paper83.pdf][2020-083]     |
| 2  | 108  | *...too many to list*         |
| 2* | 1    | [nime2015_264.pdf][2015-264]         |
| 1  | 1022 | *...too many to list*         |
| 1* | 50   | ***...maybe worth scanning*** | 

Notes: 

- 2: 108 papers had `environment` and 1 other keyword. These are ignored.
- 2*: 1 paper didn't have `environment` but had 2 other keywords. This is included
- 1: 1022 papers only have the word `environment` and are ignored. 
- 1*: 50 papers have one keyword that is not `environment`. These are excluded for now, but if we wanted, could scan through. 
- For full list with paper IDs, see [data/data_20201208.xlsx](data/data_20201208.xlsx) spreadsheet (1st sheet: RESULTS)

----

## For  a quick overview, we focus on these 16 papers: 

### 5 keywords: 

- Magnusson, T., Eldridge, A., & Kiefer, C. (2020). Instrumental Investigations at the Emute Lab. [link][2020-097]

### 4 keywords: 

- Freed, A. (2012). The Fingerphone: A Case Study of Sustainable Instrument Redesign. [link][2012-264]
- Brown, C., & Paine, G. (2015). Rawr ! Study in Sonic Skulls : Embodied Natural History. [link][2015-325] 
- Baalman, M. A. (2017). Wireless Sensing for Artistic Applications, a Reflection on Sense/Stage to Motivate the Design of the Next Stage. [link][2017-059] 
- Morreale, F., Bin, S. M. A., McPherson, A., Stapleton, P., & Wanderley, M. (2020). A NIME Of The Times: Developing an Outward-Looking Political Agenda For This Community. [link][2020-031] 

### 3 keywords: 

- Young, D., Nunn, P., & Vassiliev, A. (2006). Composing for Hyperbow: A Collaboration Between MIT and the Royal Academy of Music. [link][2006-396]
- Fraietta, A. (2008). Open Sound Control: Constraints and Limitations. [link][2008-019]
- Stahl, A., & Clemens, P. (2010). Auditory Masquing : Wearable Sound Systems for Diegetic Character Voices. [link][2010-427]
- Smallwood, S. (2011). Solar Sound Arts: Creating Instruments and Devices Powered by Photovoltaic Technologies. [link][2011-028]
- Lynch, E., & Paradiso, J. (2016). SensorChimes: Musical Mapping for Sensor Networks. [link][2016-028]
- Gurevich, M. (2017). Discovering Instruments in Scores: A Repertoire-Driven Approach to Designing New Interfaces for Musical Expression. [link][2017-031]
- Marquez-Borbon, A., Pablo, J., & Avila, M. (2018). The Problem of DMI Adoption and Longevity: Envisioning a NIME Performance Pedagogy. [link][2018-040]
- Lepri, G., & Mcpherson, A. (2019). Fictional Instruments , Real Values : Discovering Musical Backgrounds with Non-Functional Prototypes. [link][2019-024]
- Lucas, A., Schroeder, F., & Ortiz, M. (2020). The Longevity of Bespoke , Accessible Music Technology : A Case for Community. [link][2020-046]
- Hayes, L., & Marquez-Borbon, A. (2020). Nuanced and Interrelated Mediations and Exigencies (NIME): Addressing the Prevailing Political and Epistemological Crises. [link][2020-083]

### 2 keywords: 

- Benford, S., Hazzard, A., Chamberlain, A., & Xu, L. (2015). Augmenting a Guitar with its Digital Footprint. [link][2015-264] 



[2020-097]: papers_for_review/5_nime2020_paper97.pdf
[2012-264]: papers_for_review/4_nime2012_264.pdf
[2015-325]: papers_for_review/4_nime2015_325.pdf
[2017-059]: papers_for_review/4_nime2017_paper0059.pdf
[2020-031]: papers_for_review/4_nime2020_paper31.pdf
[2006-396]: papers_for_review/3_nime2006_396.pdf
[2008-019]: papers_for_review/3_nime2008_019.pdf
[2010-427]: papers_for_review/3_nime2010_427.pdf
[2011-028]: papers_for_review/3_nime2011_028.pdf
[2016-028]: papers_for_review/3_nime2016_paper0028.pdf
[2017-031]: papers_for_review/3_nime2017_paper0031.pdf
[2018-040]: papers_for_review/3_nime2018_paper0040.pdf
[2019-024]: papers_for_review/3_nime2019_paper024.pdf
[2020-046]: papers_for_review/3_nime2020_paper46.pdf
[2020-083]: papers_for_review/3_nime2020_paper83.pdf
[2015-264]: papers_for_review/2_nime2015_264.pdf