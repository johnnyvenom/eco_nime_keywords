Keywords to search for:

- environment
- sustainability
- conservation
- ecology
- pollution
- carbon
- footprint
- biodiversity
- climate

NOTES: 

- of course, these terms will be used in various other contexts that have little or nothing to do with environmental issues. Thus we should follow with collocation/concordance analyses AND manual inspection. 
- probably worth searching for other forms and endings: environment, environmental, environmentally, etc. Maybe figure out a smart way to do this (in script)