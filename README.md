NIME 2020 paper: eco.nime
=========================

This repo contains assets and tools for a meta review of NIME proceedings to see if and where environmental issues have been addressed previously in NIME research. 

For current analysis documentation, please see [notes.md](notes.md)
## Methods: 

- automated keyword search of downloaded proceedings, using approach by Jensenius (NIME 2014) to investigate 'gesture', and adapted by Sullivan & Wanderley (SMC 2018) to explore DMI stability. 
- collocation and concordance analyses to understand context?

It might be good to run the keyword search first, then examine the papers/abstracts manually after. 

See [KEYWORDS.md](keywords.md) for list of keywords to search for.
## Keyword script usage: 

The keywords search script is run in the [NIME_proceedings_pdf](NIME_proceedings_pdf) directory, which contains all papers by year. **(The actual pdfs are `.gitignore`ed in the remote repo, so you will need to download them yourself from https://www.uio.no/ritmo/english/people/management/alexanje/research/nime/proceedings/) and unzip them into .pdfs in subdirectories by year):** 

    _ NIME_proceedings_pdf
        |_ 2001
            |_ nime2001_003.pdf
            |_ nime2001_007.pdf
            |_ ...etc.
        |_ 2002
            |_ ...etc.
        |_ 2003
            |_ ...etc.
        |_ ...etc.
        |_ keywords.sh

From the command line, run: 

    cd NIME_proceedings_pdf
    ./keyword.sh keyword1 keyword2 keyword3 etc.

The script creates a `data.csv` file with results by year (number of occurrences for each term, and list of papers). Note that it gets overwritten each time it is run. 